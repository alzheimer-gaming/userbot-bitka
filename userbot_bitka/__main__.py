import asyncio
from pyrogram.client import Client


async def main():
    api_id = int(input("api_id: "))
    api_hash = input("api_hash: ")
    app = Client("my_account", api_id=api_id, api_hash=api_hash, hide_password=True)
    await app.start()
    print("")
    print(await app.export_session_string())
    await app.stop()

asyncio.run(main())
