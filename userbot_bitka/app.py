from pyrogram.client import Client
from fastapi import FastAPI

from userbot_bitka.config import settings


fastapi_app = FastAPI()
app = Client("userbot_bitka", session_string=settings.tg_session_string)


@fastapi_app.post("/")
async def change_username(new_name: str):
    await app.start()
    await app.send_message("BotFather", "/cancel")
    await app.send_message("BotFather", "/setname")
    await app.send_message("BotFather", settings.posting_bot_username)
    await app.send_message("BotFather", new_name)
    await app.stop()
