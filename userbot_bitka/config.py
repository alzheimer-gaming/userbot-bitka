from pydantic import BaseSettings


class Settings(BaseSettings):
    tg_session_string: str
    posting_bot_username: str


settings = Settings()
